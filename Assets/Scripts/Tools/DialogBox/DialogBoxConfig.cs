using System;

[System.Serializable]
public class DialogBoxConfig
{
    public string Message;
    public string AcceptBtnText;
    public bool WithoutRejectBtn;
    public string RejectBtnText;
    public Action<bool> OnPressedBtnCallback;
}
