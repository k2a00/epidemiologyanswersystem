using System;
using UnityEngine;
using UnityEngine.UI;

public class InputElementsCreator : MonoBehaviour
{
    [SerializeField] private InputElement inputElementPrefab;
    [SerializeField] private Transform parent;


    public void InstantiateInputElements(string[] entry, string[] tittle, Action<Text> callback)
    {
        for (int i = 0; i < entry.Length; i++)
        {
            InputElement inputElement = Instantiate(inputElementPrefab, parent);

            inputElement.SetTittle(tittle[i]);

            callback(inputElement.GetInputTextComponent());
        }
    }
}
