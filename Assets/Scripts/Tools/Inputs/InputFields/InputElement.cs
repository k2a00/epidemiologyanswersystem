using UnityEngine;
using UnityEngine.UI;

public class InputElement : MonoBehaviour
{
    [SerializeField] private Text inputData;
    [SerializeField] private Text tittle;


    public void SetTittle(string value)
    {
        tittle.text = value;
    }

    public Text GetInputTextComponent()
    {
        return inputData;
    }
}
