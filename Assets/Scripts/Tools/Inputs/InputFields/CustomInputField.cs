using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine;

public class CustomInputField : InputField, IPointerClickHandler
{

    private int lastCaretPosition = -1;


    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);

        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            return;
        }
        
        CustomKeyboard.Instance.Show(this);
    }
    
    public void AddChar(string value)
    {
        lastCaretPosition ++;
        
        text = text.Insert(lastCaretPosition, value);

        Select();
        
        StartCoroutine(WaitFrame());
    }

    public void MoveCaretLeft()
    {
        if (lastCaretPosition == -1)
        {
            StartCoroutine(WaitFrame());

            return;
        }

        caretPosition = lastCaretPosition;
        
        lastCaretPosition--;

        StartCoroutine(WaitFrame());
    }

    public void MoveCaretRight()
    {
        if (lastCaretPosition == text.Length - 1)
        {
            StartCoroutine(WaitFrame());

            return;
        }

        lastCaretPosition++;

        caretPosition = lastCaretPosition + 1;

        StartCoroutine(WaitFrame());
    }

    public void DeleteChar()
    {
        if (lastCaretPosition == -1)
        {
            return;
        }

        text = text.Remove(lastCaretPosition, 1);

        lastCaretPosition--;

        StartCoroutine(WaitFrame());
    }

    private IEnumerator WaitFrame()
    {
        Select();

        yield return null;

        MoveTextEnd(false);

        caretPosition = lastCaretPosition + 1;
    }
}

