using System;
using UnityEngine;
using UnityEngine.UI;


public class VariantSelector : MonoBehaviour
{
    [SerializeField] private Text view;
    [SerializeField] private string postfix;
    public event Action<int> OnSelectVariant;

    private int variantNumber;


    public void SetVariantNumber(int number)
    {
        variantNumber = number;

        view.text = (number + 1).ToString() + postfix;
    }

    public void SetValues(int index, string tittle)
    {
        variantNumber = index;

        view.text = tittle;
    }

    public void Select()
    {
        OnSelectVariant?.Invoke(variantNumber);
    }
}
