using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesLoader : MonoBehaviour
{
    public enum Scenes { Auth, MainMenu, Notice, InteractiveRoom, Evaluations }


    public void LoadScene(Scenes scene)
    {
        SceneManager.LoadScene(scene.ToString());
    }
}
