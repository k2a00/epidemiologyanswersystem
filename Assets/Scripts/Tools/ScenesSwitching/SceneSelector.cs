using UnityEngine;

public class SceneSelector : MonoBehaviour
{
    [SerializeField] private ScenesLoader.Scenes sceneForLoad;
    [SerializeField] private ScenesLoader scenesLoader;


    public void LoadSelecedScene()
    {
        scenesLoader.LoadScene(sceneForLoad);
    }
}
