﻿using System.Collections.Generic;
using UnityEngine;

public class SwitchingGroup : MonoBehaviour
{
    [SerializeField] private List<SwitchingButton> switchingButtons;

    private GameObject activeGO;


    private void Start()
    {
        for (int i = 0; i < switchingButtons.Count; i++)
        {
            switchingButtons[i].OnNeedSwitch += SwitchToAppointedGO;
        }
    }

    public void DisableActiveElement()
    {
        activeGO.SetActive(false);
    }
   
    private void SwitchToAppointedGO(GameObject currentSwitchingElement)
    {
        if (activeGO != null)
        {
            activeGO.SetActive(false);
        }
        Debug.Log("group");
        currentSwitchingElement.SetActive(true);

        activeGO = currentSwitchingElement;
    }
}