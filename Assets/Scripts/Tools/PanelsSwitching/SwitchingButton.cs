﻿using System;
using UnityEngine;

public class SwitchingButton : MonoBehaviour
{
    [SerializeField] private GameObject switchingGO;

    public event Action<GameObject> OnNeedSwitch;


    public void SwitchToAppointedGO()
    {
        OnNeedSwitch?.Invoke(switchingGO);
    }
}
