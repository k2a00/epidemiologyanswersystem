using System.Collections.Generic;
using UnityEngine;

public class ValuesRandomizer
{
    private readonly string englishAlphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    

    public string GetString(int length)
    {
        string randomString = "";

        for (int i = 0; i < length; i++)
        {
            randomString += englishAlphabet[Random.Range(0, englishAlphabet.Length)];
        }

        return randomString;
    }

    public List<int> GetUniqueIndexes(int countOfNeededIndexes, int collectionCount)
    {
        if(countOfNeededIndexes > collectionCount)
        {
            Debug.LogError("countOfNeededIndexes > collectionCount");

            return null;
        }

        int currentIndex;
        bool wasIndexCheck;
        List<int> indexes = new List<int>();

        for (int i = 0; i < countOfNeededIndexes; i++)
        {
            currentIndex = Random.Range(0, collectionCount);

            wasIndexCheck = false;

        GoToElement:
            while (!wasIndexCheck)
            {
                for (int a = 0, count = indexes.Count; a < count; a++)
                {
                    if (currentIndex == indexes[a])
                    {
                        currentIndex = Random.Range(0, collectionCount);

                        goto GoToElement;
                    }
                }

                wasIndexCheck = true;
            }

            indexes.Add(currentIndex);
        }

        return indexes;
    }
}
