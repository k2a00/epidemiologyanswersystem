using UnityEngine.UI;

public static class CanvasRedrawer
{
    public static void Redraw(CanvasScaler canvasScaler)
    {
        canvasScaler.enabled = false;
        canvasScaler.enabled = true;
    }
}
