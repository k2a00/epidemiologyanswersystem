using UnityEngine;

public class CharAsKeyboardElement : MonoBehaviour
{
    [SerializeField] private string charElement;


    public void OnPressed() 
    {
        CustomKeyboard.Instance.AddChar(charElement);
    }
}
