using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Notice : MonoBehaviour
{
    [SerializeField] private Text taskInfo;
    [SerializeField] private Text currentVariant;
    [SerializeField] private Text additionalInfo;
    [SerializeField] private Text formInfo;
    [SerializeField] private Text lawInfo;
    [SerializeField] private Text mainTittle;
    [SerializeField] private GameObject taskFormCanvas;
    [SerializeField] private GameObject sendingOverlay;
    [Space]
    [SerializeField] private InputElementsCreator inputElementsCreator;
    [SerializeField] private NoticeVariantsStorage noticeVariantsStorage;
    [SerializeField] private DialogBox dialogBox;
    [SerializeField] private ScenesLoader scenesLoader;
    
    private List<Text> inputTextComponents = new List<Text>();
    private string noticeConfigRawData;

    private readonly string taskInfoTag = "#TaskInfo";
    private readonly string inputPosTag = "#InputPos";
    private readonly string inputTittleTag = "#InputTittle";
    private readonly string mainTittleTag = "#MainTittle";
    private readonly string lawInfoTag = "#LawInfo";
    private readonly string formInfoTag = "#FormInfo";
    private readonly string additionalInfoTag = "#AdditionalInfo";
    private readonly int answerIDLength = 6;

    private SequenceElementOfUserAnswer userID;
    private SequenceElementOfUserAnswer groupNumber;
    private SequenceElementOfUserAnswer answerID;
    private SequenceElementOfUserAnswer setOfVariants;
    private string setOfVariantsID;
    private string[] variantInfo;
    private SequenceElementOfUserAnswer variant;
    private string[] inputPoss;

    Formatting formatting = new Formatting();
    private ServerCommunication serverCommunication = new ServerCommunication();


    private void Start()
    {
        FormatData();
    }

    public void GetConfirmationOfSending()
    {
        DialogBoxConfig config = new DialogBoxConfig()
        {
            Message = "��������� �� ��������?",
            AcceptBtnText = "��",
            RejectBtnText = "���",
            OnPressedBtnCallback = PrepareToSendingToServer
        };

        dialogBox.Show(config);
    }

    private async void FormatData()
    {
        noticeConfigRawData = await serverCommunication.GetNoticeConfig();

        string[] lines = formatting.SplitOnLines(noticeConfigRawData);

        userID = new SequenceElementOfUserAnswer()
        {
            CSVPos = formatting.GetSubstringAfterTag($"#{DataTags.UserID}Pos", lines),
            InputText = MainConfig.CurrentUser.UserID
        };

        groupNumber = new SequenceElementOfUserAnswer()
        {
            CSVPos = formatting.GetSubstringAfterTag($"#{DataTags.GroupNumber}Pos", lines),
            InputText = MainConfig.CurrentUser.GroupNumber
        };

        setOfVariantsID = formatting.GetSubstringAfterTag($"#{DataTags.SetOfVariants}", lines);

        setOfVariants = new SequenceElementOfUserAnswer()
        {
            CSVPos = formatting.GetSubstringAfterTag($"#{DataTags.SetOfVariants}Pos", lines),
            InputText = setOfVariantsID
        };

        variantInfo = formatting.GetCellsValues(formatting.GetSubstringAfterTag($"#{DataTags.SetOfVariants}-{setOfVariantsID}" , await serverCommunication.GetSetOfVariantsByID(setOfVariantsID)));

        noticeVariantsStorage.InstantiateVariantSelectors(variantInfo.Length, OnSelectedVariant);

        variant = new SequenceElementOfUserAnswer()
        {
            CSVPos = formatting.GetSubstringAfterTag($"#{DataTags.Variant}Pos", lines),
            InputText = ""
        };

        ValuesRandomizer valuesRandomizer = new ValuesRandomizer();

        answerID = new SequenceElementOfUserAnswer()
        {
            CSVPos = formatting.GetSubstringAfterTag($"#{DataTags.AnswerID}Pos", lines),
            InputText = valuesRandomizer.GetString(answerIDLength)
        };

        inputPoss = formatting.GetCellsValues(formatting.GetSubstringAfterTag(inputPosTag, lines));

        string[] tittles = formatting.GetCellsValues(formatting.GetSubstringAfterTag(inputTittleTag, lines));

        inputElementsCreator.InstantiateInputElements(inputPoss, tittles, AddInputTextComponent);

        taskInfo.text = formatting.GetSubstringAfterTagWithReplacing(taskInfoTag, lines);
        mainTittle.text = formatting.GetSubstringAfterTagWithReplacing(mainTittleTag, lines);
        lawInfo.text = formatting.GetSubstringAfterTagWithReplacing(lawInfoTag, lines);
        formInfo.text = formatting.GetSubstringAfterTagWithReplacing(formInfoTag, lines);
        additionalInfo.text = formatting.GetSubstringAfterTagWithReplacing(additionalInfoTag, lines);
    }

    private void PrepareToSendingToServer(bool response)
    {
        if (!response)
        {
            return;
        }

        sendingOverlay.SetActive(true);

        List<SequenceElementOfUserAnswer> configs = new List<SequenceElementOfUserAnswer>
        {
            userID,
            groupNumber,
            variant,
            answerID,
            setOfVariants
        };

        for (int i = 0; i < inputPoss.Length; i++)
        {
            SequenceElementOfUserAnswer config = new SequenceElementOfUserAnswer()
            {
                CSVPos = inputPoss[i],
                InputText = formatting.FormatByServerRules(inputTextComponents[i].text)
            };

            configs.Add(config);
        }

        serverCommunication.AddNoticeAnswer(configs, GetSendingResult);
    }

    private void AddInputTextComponent(Text inputTextComponent)
    {
        inputTextComponents.Add(inputTextComponent);
    }

    private void OnSelectedVariant(int seletedVariant)
    {
        variant.InputText = seletedVariant.ToString();

        currentVariant.text = variantInfo[seletedVariant];

        for (int i = 0; i < inputTextComponents.Count; i++)
        {
            inputTextComponents[i].text = "";
        }

        taskFormCanvas.SetActive(true);
    }

    private void GetSendingResult(bool isSent, string serverResponse)
    {
        sendingOverlay.SetActive(false);

        DialogBoxConfig config = new DialogBoxConfig()
        {
            Message = "������: ",
            AcceptBtnText = "��",
            WithoutRejectBtn = true,
            OnPressedBtnCallback = LoadMainMenuScene
        };

        if (isSent)
        {
            config.Message += "����������";
        }
        else
        {
            config.Message += serverResponse;
            config.OnPressedBtnCallback = null;
        }

        dialogBox.Show(config);
    }

    private void LoadMainMenuScene(bool response)
    {
        scenesLoader.LoadScene(ScenesLoader.Scenes.MainMenu);
    }
}