public class NoticeAnswersViewer : EvaluationViewer
{
    private int setOfVariantsIndex;
    private int variantIndex;


    protected override void FormatRawData(string rawData)
    {
        base.FormatRawData(rawData);

        setOfVariantsIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.SetOfVariantsID}", answerMask);
    }

    protected override void SetTittlesForAnswerElements()
    {
        int endIndex = answerMask.IndexOf($"#{DataTags.UserID}") - 1;

        string[] tittlesForAnswers = formatting.GetCellsValues(answerMask.Substring(0, endIndex));

        string variantTag = $"#{DataTags.Variant}";

        for (int i = 0; i < tittlesForAnswers.Length; i++)
        {
            if (tittlesForAnswers[i] == variantTag)
            {
                tittlesForAnswers[i] = "������� � ��������";

                variantIndex = i;

                break;
            }
        }

        CreateAnswerElements(tittlesForAnswers);
    }

    protected override async void OnUserAnswerSelected(int index)
    {
        string[] answerCellValues = formatting.GetCellsValues(answersLines[index]);

        FillUserAnswerFields(answerCellValues);

        string setOfVariantsID = answerCellValues[setOfVariantsIndex];

        string[] variantInfo = formatting.GetCellsValues(formatting.GetSubstringAfterTag($"#{DataTags.SetOfVariants}-{setOfVariantsID}", 
            await serverCommunication.GetSetOfVariantsByID(setOfVariantsID)));

        answerElements[variantIndex].SetAnswer(variantInfo[int.Parse(answerCellValues[variantIndex])]);

        string answerID = answerCellValues[answersPartAnswerIDIndex];

        FillEvaluationField(answerID);

        CanvasRedrawer.Redraw(canvasScaler);
    }
}