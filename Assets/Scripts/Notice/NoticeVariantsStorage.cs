using System;
using UnityEngine;

public class NoticeVariantsStorage : MonoBehaviour
{
    [SerializeField] private VariantSelector variantSelectorPrefab;
    [SerializeField] private Transform parent;

    private event Action<int> callback;


    public void InstantiateVariantSelectors(int amount, Action<int> onSelectedVariant)
    {
        callback = onSelectedVariant;

        for (int i = 0; i < amount; i++)
        {
            VariantSelector selector = Instantiate(variantSelectorPrefab, parent);

            selector.SetVariantNumber(i);

            selector.OnSelectVariant += OnSelectedVariant;
        }
    }

    private void OnSelectedVariant(int variantNumber)
    {
        callback?.Invoke(variantNumber);
    }
}
