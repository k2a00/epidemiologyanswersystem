using System;
using UnityEngine;

public class VariantsStorage : MonoBehaviour
{
    [SerializeField] private Transform parent;
    [SerializeField] private VariantSelector variantSelector;

    public event Action<int> OnSelected;

    private string[] selectorTittles;

    private event Action<GameObject> onCreated;


    public void SetCreatingValues(string[] tittles)
    {
        selectorTittles = tittles;

        Create();
    }

    public void SetCreatingValues(string[] tittles, Action<GameObject> onCreatedSelector)
    {
        selectorTittles = tittles;

        onCreated = onCreatedSelector;

        Create();
    }

    private void Create()
    {
        for (int i = 0; i < selectorTittles.Length; i++)
        {
            VariantSelector selector = Instantiate(variantSelector, parent);

            selector.SetValues(i, selectorTittles[i]);

            selector.OnSelectVariant += OnSelectedVariant;

            if (onCreated != null)
            {
                onCreated?.Invoke(selector.gameObject);
            }
        }

        onCreated = null;
    }

    private void OnSelectedVariant(int index)
    {
        OnSelected?.Invoke(index);
    }
}
