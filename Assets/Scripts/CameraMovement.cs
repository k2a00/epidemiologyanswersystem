﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] private Transform playerTransform;
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private float mouseSensitivity = 5f;

    Quaternion playerNewRot;
    Quaternion cameraNewRot;

    private bool cursorLocked;

    private void Start()
    {
        playerNewRot = playerTransform.rotation;
        cameraNewRot = cameraTransform.rotation;

        //Cursor.lockState = CursorLockMode.Locked;
    }

    private void LateUpdate()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");
        float speedRot = mouseSensitivity * Time.deltaTime;

        playerNewRot *= Quaternion.Euler(0, mouseX, 0);
        playerTransform.rotation = Quaternion.Lerp(playerTransform.rotation, playerNewRot, speedRot);

        cameraNewRot *= Quaternion.Euler(-mouseY, 0, 0);
        cameraTransform.localRotation = Quaternion.Lerp(cameraTransform.localRotation, cameraNewRot, speedRot);

        if (Input.GetKeyDown(KeyCode.C))
        {
            cursorLocked = !cursorLocked;

            ChangeCursorMode();
        }
    }

    private void ChangeCursorMode()
    {
        if (cursorLocked)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
