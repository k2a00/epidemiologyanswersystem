using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class EvaluationViewer : MonoBehaviour
{
    [SerializeField] protected TaskNames taskName;
    [SerializeField] protected GameObject loadingOverlay;
    [SerializeField] protected VariantsStorage userAllAnswersStorage;
    [SerializeField] protected AnswerElement answerElement;
    [SerializeField] protected Transform answersParent;
    [SerializeField] protected CanvasScaler canvasScaler;

    protected Formatting formatting = new Formatting();
    protected ServerCommunication serverCommunication = new ServerCommunication();

    protected List<AnswerElement> answerElements = new List<AnswerElement>();
    protected List<GameObject> userAnswersTemporaryStorage = new List<GameObject>();

    protected List<string> answersLines = new List<string>();
    protected List<string> evaluationsLines = new List<string>();
    protected string answerMask;
    protected int answersPartAnswerIDIndex;
    protected int evaluationIndex;


    protected void Start()
    {
        userAllAnswersStorage.OnSelected += OnUserAnswerSelected;

        GetRawDataOfUserAnswers();
    }

    protected async void GetRawDataOfUserAnswers()
    {
        string rawData = await serverCommunication.GetUserTaskAnswers(taskName);

        if (rawData == "")
        {
            return;
        }

        FormatRawData(rawData);
    }

    protected virtual void FormatRawData(string rawData)
    {
        loadingOverlay.SetActive(true);

        int evaluationsPartIndex = rawData.IndexOf($"#{DataTags.EvaluationPart}");

        string[] linesOfAnswersPart = formatting.SplitOnLines(rawData.Substring(0, evaluationsPartIndex - 1));

        answerMask = linesOfAnswersPart[0];

        for (int i = 1; i < linesOfAnswersPart.Length; i++)
        {
            answersLines.Add(linesOfAnswersPart[i]);
        }

        answersPartAnswerIDIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.AnswerID}", answerMask);

        string[] linesOfEvaluationPart = formatting.SplitOnLines(rawData.Substring(evaluationsPartIndex + $"#{DataTags.EvaluationPart}~#end".Length));

        evaluationIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.Evaluation}", linesOfEvaluationPart[0]);

        for (int i = 1; i < linesOfEvaluationPart.Length; i++)
        {
            evaluationsLines.Add(linesOfEvaluationPart[i]);
        }

        SetTittlesForAnswerElements();

        ShowAllUserAnswers();
    }

    protected abstract void SetTittlesForAnswerElements();

    protected void CreateAnswerElements(string[] tittlesForAnswers)
    {
        for (int i = 0; i < answerElements.Count; i++)
        {
            Destroy(answerElements[i].gameObject);
        }

        answerElements.Clear();

        for (int i = 0; i < tittlesForAnswers.Length; i++)
        {
            AnswerElement element = Instantiate(answerElement, answersParent);

            element.SetTittle(tittlesForAnswers[i]);

            answerElements.Add(element);
        }

        CreateEvaluationField();
    }

    private void CreateEvaluationField()
    {
        AnswerElement element = Instantiate(answerElement, answersParent);

        element.SetTittle("���������� ������");

        answerElements.Add(element);
    }

    private void ShowAllUserAnswers()
    {
        List<string> selectorsTittles = new List<string>();

        for (int i = 0; i < answersLines.Count; i++)
        {
            string[] cellValues = formatting.GetCellsValues(answersLines[i]);

            selectorsTittles.Add(cellValues[0]);
        }

        for (int i = 0; i < userAnswersTemporaryStorage.Count; i++)
        {
            Destroy(userAnswersTemporaryStorage[i]);
        }

        userAnswersTemporaryStorage.Clear();

        userAllAnswersStorage.SetCreatingValues(selectorsTittles.ToArray(), OnUserAnswerCreated);

        loadingOverlay.SetActive(false);
    }

    private void OnUserAnswerCreated(GameObject userAnswer)
    {
        userAnswersTemporaryStorage.Add(userAnswer);
    }

    protected abstract void OnUserAnswerSelected(int index);

    protected void FillUserAnswerFields(string[] answerCellValues)
    {
        int answerElementsWithoutEvaluationField = answerElements.Count - 1;

        for (int a = 0; a < answerElementsWithoutEvaluationField; a++)
        {
            answerElements[a].SetAnswer(formatting.CheckLineBreaks(answerCellValues[a]));
        }
    }

    protected void FillEvaluationField(string answerID)
    {
        string evaluation = "";

        for (int i = 0, count = evaluationsLines.Count; i < count; i++)
        {
            if (evaluationsLines[i].IndexOf($"{answerID}~") != -1)
            {
                evaluation = formatting.GetCellsValues(evaluationsLines[i])[evaluationIndex];

                break;
            }
        }

        answerElements[answerElements.Count - 1].SetAnswer(formatting.CheckLineBreaks(evaluation));
    }
}