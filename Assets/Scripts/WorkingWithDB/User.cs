[System.Serializable]
public class User 
{
    public string UserID;
    public string LastName;
    public string FirstName;
    public string Patronymic;
    public string GroupNumber;
}
