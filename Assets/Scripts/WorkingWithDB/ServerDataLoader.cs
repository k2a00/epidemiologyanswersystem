using System.Threading.Tasks;
using UnityEngine.Networking;

public class ServerDataLoader
{
    public async Task<string> DownloadByURL(string url)
    {
        UnityWebRequest request = UnityWebRequest.Get(url);
    
        request.certificateHandler = new BypassCertificate();
           
        request.SendWebRequest();

        while (!request.isDone)
        {
            await Task.Yield();
        }

        return request.downloadHandler.text;
    }
}