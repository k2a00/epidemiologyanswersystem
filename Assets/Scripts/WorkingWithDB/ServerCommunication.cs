using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ServerCommunication
{
    private readonly string indexURL = "http://f0667571.xsph.ru/Epidemiology/";

    private enum phpKeyWords
    {
        Function,
        Indicator,
        UserID, 
        SetOfVariantsID,
        UserAnswer,
        RoomName,
        TaskName,
        Successfully
    }

    private enum phpFunctions
    {
        GetUserTaskAnswers,
        AddNoticeAnswer, 
        AddViolationsRoomAnswer,
        GetSetOfVariantsByID,
        GetNoticeConfig,
        GetViolationsRoomConfig,
        CheckUserID
    }


    public async Task<string> CheckUserID(string prefixUserID, string userID)
    {
        WWWForm form = new WWWForm();

        form.AddField(phpKeyWords.Function.ToString(), phpFunctions.CheckUserID.ToString());
        form.AddField(phpKeyWords.Indicator.ToString(), prefixUserID);
        form.AddField(phpKeyWords.UserID.ToString(), userID);

        return await SendRequest(form);
    }

    public async Task<string> GetNoticeConfig()
    {
        WWWForm form = new WWWForm();

        form.AddField(phpKeyWords.Function.ToString(), phpFunctions.GetNoticeConfig.ToString());

        return await SendRequest(form);
    }

    public async Task<string> GetViolationsRoomConfig(string roomName)
    {
        WWWForm form = new WWWForm();

        form.AddField(phpKeyWords.Function.ToString(), phpFunctions.GetViolationsRoomConfig.ToString());
        form.AddField(phpKeyWords.RoomName.ToString(), roomName);

        return await SendRequest(form);
    }

    public async Task<string> GetUserTaskAnswers(TaskNames taskName)
    {
        WWWForm form = new WWWForm();

        form.AddField(phpKeyWords.Function.ToString(), phpFunctions.GetUserTaskAnswers.ToString());
        form.AddField(phpKeyWords.TaskName.ToString(), taskName.ToString());
        form.AddField(phpKeyWords.Indicator.ToString(), MainConfig.UserPrefix);
        form.AddField(phpKeyWords.UserID.ToString(), MainConfig.CurrentUser.UserID);

        return await SendRequest(form);
    }

    public async Task<string> GetSetOfVariantsByID(string setID)
    {
        WWWForm form = new WWWForm();

        form.AddField(phpKeyWords.Function.ToString(), phpFunctions.GetSetOfVariantsByID.ToString());
        form.AddField(phpKeyWords.SetOfVariantsID.ToString(), setID);

        return await SendRequest(form);
    }

    public async void AddNoticeAnswer(List<SequenceElementOfUserAnswer> configsForSending, Action<bool, string> callback)
    {
        string userAnswer = "";
        
        int configsCount = configsForSending.Count;

        WWWForm form = new WWWForm();
        
        SequenceElementOfUserAnswer[] validConfigs = new SequenceElementOfUserAnswer[configsCount];

        for (int i = 0; i < configsCount; i++)
        {
            for (int a = 0; a < configsCount; a++)
            {
                if (int.Parse(configsForSending[a].CSVPos) == i)
                {
                    validConfigs[i] = configsForSending[a];

                    break;
                }
            }
        }
       
        for (int i = 0; i < configsCount; i++)
        {
            userAnswer += $"{validConfigs[i].InputText}~";
        }

        form.AddField(phpKeyWords.Function.ToString(), phpFunctions.AddNoticeAnswer.ToString());
        form.AddField(phpKeyWords.Indicator.ToString(), MainConfig.UserPrefix);
        form.AddField(phpKeyWords.UserAnswer.ToString(), userAnswer);

        ProcessServerResponse(await SendRequest(form), callback);
    }


    public async void AddViolationsRoomAnswer(string answer, string roomName, Action<bool, string> callback)
    {
        WWWForm form = new WWWForm();

        form.AddField(phpKeyWords.Function.ToString(), phpFunctions.AddViolationsRoomAnswer.ToString());
        form.AddField(phpKeyWords.Indicator.ToString(), MainConfig.UserPrefix);
        form.AddField(phpKeyWords.RoomName.ToString(), roomName);
        form.AddField(phpKeyWords.UserAnswer.ToString(), answer);

        ProcessServerResponse(await SendRequest(form), callback);
    }

    private async Task<string> SendRequest(WWWForm form)
    {
        UnityWebRequest request = UnityWebRequest.Post(indexURL, form);

        request.SendWebRequest();

        while (!request.isDone)
        {
            await Task.Yield();
        }

        return request.downloadHandler.text;
    }

    private void ProcessServerResponse(string serverResponse, Action<bool, string> callback)
    {
        if (serverResponse == phpKeyWords.Successfully.ToString())
        {
            callback?.Invoke(true, "");
        }
        else
        {
            callback?.Invoke(false, serverResponse);
        }
    }
}
