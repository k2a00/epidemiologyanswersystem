using UnityEngine;
using UnityEngine.UI;


public class UserIdentification : MonoBehaviour
{
    [SerializeField] private Button loginBtn;
    [SerializeField] private Text userIDInputField;
    [SerializeField] private Text notice;
    [Space]
    [SerializeField] private DialogBox dialogBox;
    [SerializeField] private ScenesLoader scenesLoader;

    private User currentUser;


    public async void TryLogin()
    {
        if (userIDInputField.text == "")
        {
            return;
        }

        string input = userIDInputField.text;

        int prefixEndIndex = input.IndexOf('-');

        if (prefixEndIndex == -1)
        {
            notice.text = "������������� �� ������";

            return;
        }

        loginBtn.interactable = false;

        string userPrefix = input.Substring(0, prefixEndIndex).Trim();

        ServerCommunication serverCommunication = new ServerCommunication();

        string userInfo = await serverCommunication.CheckUserID(userPrefix, input);

        if (userInfo == "")
        {
            notice.text = "������������� �� ������";

            loginBtn.interactable = true;

            return;
        }

        MainConfig.UserPrefix = userPrefix;

        FillUserData(userInfo);
    }

    private void FillUserData(string userRawData)
    {
        Formatting formatting = new Formatting();

        string[] lines = formatting.SplitOnLines(userRawData);

        string mask = lines[0];

        string[] cellsOfUserInfo = formatting.GetCellsValues(lines[1]);

        currentUser = new User()
        {
            UserID = cellsOfUserInfo[formatting.GetCellIndexThatBeginWith($"#{DataTags.UserID}", mask)],
            LastName = cellsOfUserInfo[formatting.GetCellIndexThatBeginWith($"#{DataTags.LastName}", mask)],
            FirstName = cellsOfUserInfo[formatting.GetCellIndexThatBeginWith($"#{DataTags.FirstName}", mask)],
            Patronymic = cellsOfUserInfo[formatting.GetCellIndexThatBeginWith($"#{DataTags.Patronymic}", mask)],
            GroupNumber = cellsOfUserInfo[formatting.GetCellIndexThatBeginWith($"#{DataTags.GroupNumber}", mask)]
        };

        DialogBoxConfig config = new DialogBoxConfig()
        {
            Message = $"{currentUser.LastName} {currentUser.FirstName} {currentUser.Patronymic}, {currentUser.GroupNumber}. ��� ���� ������?",
            AcceptBtnText = "��",
            RejectBtnText = "���",
            OnPressedBtnCallback = OnDialogBoxResponsed
        };

        dialogBox.Show(config);
    }

    private void OnDialogBoxResponsed(bool response)
    {
        if (response)
        {
            LoginAsCurrentUser();
        }
        else
        {
            loginBtn.interactable = true;

            DialogBoxConfig config = new DialogBoxConfig()
            {
                Message = $"������� ������������� ��� ���, ��������� ������������ �����",
                AcceptBtnText = "��",
                WithoutRejectBtn = true
            };

            dialogBox.Show(config);
        }
    }

    private void LoginAsCurrentUser()
    {
        MainConfig.CurrentUser = currentUser;

        scenesLoader.LoadScene(ScenesLoader.Scenes.MainMenu);
    }
}