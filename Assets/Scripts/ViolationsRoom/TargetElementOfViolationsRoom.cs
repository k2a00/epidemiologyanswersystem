using UnityEngine;

[System.Serializable]
public class TargetElementOfViolationsRoom
{
    [SerializeField] private Transform parent;
    public Transform Parent => parent;

    [Space]
    [SerializeField] private GameObject goodPrefab;
    public GameObject GoodPrefab => goodPrefab;

    [SerializeField] private GameObject[] badPrefabs;
    public GameObject[] BadPrefab => badPrefabs;

    [Space]
    [SerializeField] private int[] violationID;
    public int[] ViolationID => violationID;
}
