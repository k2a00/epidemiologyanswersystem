using System.Collections.Generic;
using UnityEngine;

public class ViolationsRoom : MonoBehaviour
{
    [SerializeField] private RoomNames roomName;
    [SerializeField] private DialogBox dialogBox;
    [SerializeField] private ViolationsRoomFiller filler;
    [SerializeField] private ViolationsRoomAnswerSender violationsRoomAnswerSender;    

    private string violationsID = "";

    private readonly char separator = '|';


    private void Start()
    {
        LaunchRoom();
    }

    private async void LaunchRoom()
    {
        ServerCommunication serverCommunication = new ServerCommunication();

        string rawData = await serverCommunication.GetViolationsRoomConfig(roomName.ToString());

        Formatting formatting = new Formatting();

        int violationsCount = int.Parse(formatting.GetSubstringAfterTag($"#{DataTags.ViolationsCount}", rawData));

        List<int> violationsIDList = filler.Fill(violationsCount);

        violationsID += $"{violationsIDList[0]}";

        for (int i = 1; i < violationsIDList.Count; i++)
        {
            violationsID += $"{separator}{violationsIDList[i]}";
        }
    }

    public void GetConfirmationOfSending()
    {
        DialogBoxConfig config = new DialogBoxConfig()
        {
            Message = "��������� �� ��������?",
            AcceptBtnText = "��",
            RejectBtnText = "���",
            OnPressedBtnCallback = SendAnswer
        };

        dialogBox.Show(config);
    }

    private void SendAnswer(bool userResponse)
    {
        if (userResponse)
        {
            violationsRoomAnswerSender.SendAnswer(violationsID, roomName.ToString());
        }
    }
}
