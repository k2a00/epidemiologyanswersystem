using System.Collections.Generic;
using UnityEngine;

public class ViolationsRoomFiller : MonoBehaviour
{
    [SerializeField] private List<TargetElementOfViolationsRoom> targetElements;

    private List<GameObject> instantiatedTargetElements = new List<GameObject>();



    public List<int> Fill(int violationsCount)
    {
        ValuesRandomizer valuesRandomizer = new ValuesRandomizer();

        List<int> violationIndexes = valuesRandomizer.GetUniqueIndexes(violationsCount, targetElements.Count);

        List<int> violationsID = new List<int>();

        for (int i = 0; i < targetElements.Count; i++)
        {
            GameObject go;
            
            TargetElementOfViolationsRoom targetElement = targetElements[i];
            
            for (int a = 0; a < violationsCount; a++)
            {
                if (i == violationIndexes[a])
                {
                    int indexOfVariantPrefab = Random.Range(0, targetElement.BadPrefab.Length);

                    go = Instantiate(targetElement.BadPrefab[indexOfVariantPrefab], targetElement.Parent);

                    violationsID.Add(targetElement.ViolationID[indexOfVariantPrefab]);

                    goto GoToElement;
                }    
            }

            if (!targetElement.GoodPrefab)
            {
                continue;
            }

            go = Instantiate(targetElement.GoodPrefab, targetElement.Parent);
            
        GoToElement:
            instantiatedTargetElements.Add(go);
        }

        return violationsID;
    }
}
