using UnityEngine;
using UnityEngine.UI;

public class ViolationsRoomAnswerSender : MonoBehaviour
{
    [SerializeField] private Text inputAnswer;
    [SerializeField] private DialogBox dialogBox;
    [SerializeField] private SceneSelector sceneSelector;
    [SerializeField] GameObject sendingOverlay;

    private readonly int answerIDLength = 6;


    public void SendAnswer(string violationsID, string roomName)
    {
        sendingOverlay.SetActive(true);

        ValuesRandomizer valuesRandomizer = new ValuesRandomizer();

        string answerID = valuesRandomizer.GetString(answerIDLength);

        string userID = MainConfig.CurrentUser.UserID;

        string userGroup = MainConfig.CurrentUser.GroupNumber;

        Formatting formatting = new Formatting();

        string userAnswer = formatting.FormatByServerRules(inputAnswer.text);

        string fullAnswer = $"{userID}~{userGroup}~{answerID}~{violationsID}~{userAnswer}~";

        ServerCommunication serverCommunication = new ServerCommunication();

        serverCommunication.AddViolationsRoomAnswer(fullAnswer, roomName , GetSendingResult);
    }

    private void GetSendingResult(bool isSent, string serverResponse)
    {
        sendingOverlay.SetActive(false);

        DialogBoxConfig config = new DialogBoxConfig()
        {
            Message = "������: ",
            AcceptBtnText = "��",
            WithoutRejectBtn = true,
            OnPressedBtnCallback = LoadMainMenuScene
        };

        if (isSent)
        {
            config.Message += "����������";
        }
        else
        {
            config.Message += serverResponse;
            config.OnPressedBtnCallback = null;
        }

        dialogBox.Show(config);
    }

    private void LoadMainMenuScene(bool response)
    {
        sceneSelector.LoadSelecedScene();
    }
}
