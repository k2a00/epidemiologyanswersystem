using UnityEngine;

public class AnswerViolationsRoomViewer : EvaluationViewer
{
    [SerializeField] protected RoomNames roomName;

    private string[] violationsID;
    private string[] violationsSummary;
    private int userAnswerIndex;
    private int violationsIDIndex;
    private int timeMarkIndex;


    protected override async void FormatRawData(string rawData)
    {
        base.FormatRawData(rawData);

        timeMarkIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.TimeMark}", answerMask);
        userAnswerIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.UserAnswer}", answerMask);
        violationsIDIndex = formatting.GetCellIndexThatBeginWith($"#{DataTags.ViolationsID}", answerMask);

        string configRawData = await serverCommunication.GetViolationsRoomConfig(roomName.ToString());

        violationsID = formatting.GetCellsValues(formatting.GetSubstringAfterTag($"#{DataTags.ViolationsID}", configRawData));
        violationsSummary = formatting.GetCellsValues(formatting.GetSubstringAfterTag($"#{DataTags.ViolationsSummary}", configRawData));
    }

    protected override void SetTittlesForAnswerElements()
    {
        string[] tittlesForAnswers = new string[] { "��������� �������", "�������� ���������", "�����" };

        CreateAnswerElements(tittlesForAnswers);
    }

    protected override void OnUserAnswerSelected(int index)
    {
        string[] answerLineCellValues = formatting.GetCellsValues(answersLines[index]);

        string[] neededPartOfAnswerLine = new string[3];

        neededPartOfAnswerLine[0] = answerLineCellValues[timeMarkIndex];

        string[] currentViolationsID = answerLineCellValues[violationsIDIndex].Split('|');

        string summary = "";

        int orderNumber = 1;

        for (int i = 0, length = currentViolationsID.Length; i < length; i++)
        {
            for (int a = 0, violationsIDLength = violationsID.Length; a < violationsIDLength; a++)
            {
                if (currentViolationsID[i] == violationsID[a])
                {
                    summary += $"{orderNumber}) {violationsSummary[a]}. \n";

                    orderNumber++;

                    break;
                }
            }
        }

        neededPartOfAnswerLine[1] = summary;
        neededPartOfAnswerLine[2] = answerLineCellValues[userAnswerIndex];

        FillUserAnswerFields(neededPartOfAnswerLine);

        string answerID = answerLineCellValues[answersPartAnswerIDIndex];

        FillEvaluationField(answerID);

        CanvasRedrawer.Redraw(canvasScaler);
    }
}